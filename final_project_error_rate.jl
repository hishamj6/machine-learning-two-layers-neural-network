using Distributions, Random, Plots, LinearAlgebra, HDF5

# linear activation function
hlin(a) = a
# and it's derivative
hplin(a) = 1

# sigmoid activation function
hsig(a) = 1 ./ (1 .+ MathConstants.e.^(-a))
# and it's derivative
hpsig(a) = hsig(a) .* (1 .- hsig(a))

# activation function for output layer
E = 10^-8
hsoftmax(a) = MathConstants.e.^(a)/ sum(MathConstants.e.^(a) .+ E)
ID = Matrix(I, 10, 10)
hpsoftmax(a) = hsoftmax(a) .* (ID .- hsoftmax(a)') 

hout = hsoftmax
hpout = hpsoftmax

# activation function for hidden layer
hhid = hsig
hphid = hpsig



l = Float64[]
i = []
h5open("mnist.h5", "r") do file
    labels = read(file, "train/labels")
    images = read(file, "train/images")
    len = size(labels)[1]
    for x = 1:len
        push!(l,labels[x])
        img = images[:,:,x]
        new_img = reshape(img, 784)
        push!(i,new_img)
    end
end

function error_rate(input, target, weight1, weight2)
    N = length(target)
    D = length(input[1])
    M = size(weight1)[1]
    final = 0
    error = 0
    for n = 1:N
        #println(N)
        
        x = input[n]
        t = target[n]

       
        # forward propagate
        y = zeros(M+1)

        y[1] = 1 # bias node
        
        a = weight1 * x[:]
        y[2:end] = hhid(a)
        a = 0
        
        a = weight2[:,:] * y[:]
        z = hout(a) 
        final =  Int(argmax(z))
        #println(z)
        #println(argmax(z) - 1, " ", Int(t))
        if (final-1) != Int(t)
            error = error + 1
        end
    end
    println(N)
    return error/N
end

h5open("weights.h5", "r") do file
    w1 = read(file, "weight1")
    w2 = read(file, "weight2")
    err = error_rate(i, l , w1, w2)
    print("Error rate ")
    println(err)
end
