using Distributions, HDF5
n = 100
sample = rand(n)
d = Normal(0.0, sqrt(2.0))
#println(d)
for (index, value) in enumerate(sample)
        #println("$index $value")
        if value <= .25
            global sample[index] = 0
        else
            global sample[index] = 1
        end
end

E = rand(d, 100)
Y = sample + 5.0*E
M = hcat(sample, Y)
h5write("simplesim.h5", "simplesim.h5", M)
r = h5read("simplesim.h5", "simplesim.h5")
#println(size(r))