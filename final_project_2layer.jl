using Distributions, Random, Plots, LinearAlgebra, HDF5


h(x) = 2*abs(x-0.5);


# linear activation function
hlin(a) = a
# and it's derivative
hplin(a) = 1

# sigmoid activation function
hsig(a) = 1 ./ (1 .+ MathConstants.e.^(-a))
# and it's derivative
hpsig(a) = hsig(a) .* (1 .- hsig(a))

# activation function for output layer
E = 10^-8
hsoftmax(a) = MathConstants.e.^(a)/ sum(MathConstants.e.^(a) .+ E)
ID = Matrix(I, 10, 10)
hpsoftmax(a) = hsoftmax(a) .* (ID .- hsoftmax(a)') 

hout = hsoftmax
hpout = hpsoftmax

# activation function for hidden layer
hhid = hsig
hphid = hpsig

function test(weight1, weight2, input, target, idx)

    N = length(idx)
    D = length(input[1])
    M = size(weight1)[1]
   
    error = 0
    for n = 1:N
       
        x = input[idx[n]]
        t = target[idx[n]]

        vec_t = zeros(10)
        vec_t[Int(t)+1] = 1

        # forward propagate
        y = zeros(M+1)

        y[1] = 1 # bias node
        
        a = weight1 * x[:]
        y[2:end] = hhid(a)
        a = 0
        
        a = weight2[:,:] * y[:]
        z = hout(a)
        
        
        error += sum((z - vec_t).^2)
    end

    return error
end

function train(input, target)
    alpha = 0.001
    beta1 = 0.9
    beta2 = 0.999
    E = 10^-8
    output_dim = 10
    # number of samples
    N = length(target) # 12665

    # dimension of input
    D = length(input[1]) # 1

    # number to hold out
    Nhold = round(Int64, N/3) #20000
    
    # number in training set
    Ntrain = N - Nhold

    # create indices
    idx = shuffle(1:N)

    trainidx = idx[1:Ntrain]
    testidx = idx[(Ntrain+1):N]

    println("$(length(trainidx)) training samples")
    println("$(length(testidx)) validation samples")
    
    # number of hidden nodes
    M = 300 

    # batch size
    B = 256
    
    # input layer activation
    inputnode = zeros(D)

    # hidden layer activation
    hiddennode = zeros(M)

    # output node activation
    outputnode = zeros(10) 

    # layer 1 weights
    weight1 = .01*randn(M, D)
    bestweight1 = weight1
    

    # layer 2 weights (include bias)
    weight2 = .01*randn(output_dim,M+1)
    bestweight2 = weight2

    numweights = prod(size(weight1)) + prod(size(weight2))
    println("$(numweights) weights")

    pdf = Uniform(1,Ntrain)

    error = []
    
    stop = false
    m1 = zeros(M, D)
    m2 = zeros(output_dim,M+1)
    v1 = zeros(M, D)
    v2 = zeros(output_dim,M+1)
    mhat1 = zeros(M, D)
    mhat2 = zeros(output_dim,M+1)
    vhat1 = zeros(M, D)
    vhat2 = zeros(output_dim,M+1)
    index = 1
    max_iteration = 1000
    while !stop
        println("Current while Index : ", index)

        # Declare the two guardians dimensions
        grad1 = zeros(M, D)
        grad2 = zeros(output_dim,M+1)

        for n = 1:B
            
            sample = trainidx[round(Int64, rand(pdf, 1)[1])]
            x = input[sample]
            t = target[sample]

            # forward propagate
            inputnode = x
            y = zeros(M+1)

            y[1] = 1 # bias node
            hiddennode = weight1 * inputnode
            y[2:end] = hhid(hiddennode[:])
            outputnode = weight2[:, 1:end] * y[:]
            
            z = hout(outputnode)
            
            #One_hot coded vector for the expected value
            vec_ten = zeros(10)
            vec_ten[Int(t)+1] = 1
            delta = z-vec_ten
            

            # compute layer 2 gradients by backpropagation of delta
            grad2[:,1] += delta*y[1]'
            grad2[:,2:end] = ((delta*y[2:end]')'* hpout(z))'
            # compute layer 1 gradients by backpropagation of delta
            for i = 1:D
                for j = 1:M
                    grad1[j,i] += delta'*weight2[:,j+1]*hphid(hiddennode[j])*x[i]
                end
            end
        end

        grad1 /= B
        grad2 /= B
        
        # update layer 1 weights
        m1 = (beta1 * m1) + ((1-beta1) * grad1)
        v1 = (beta2* v1) + ((1-beta2) * (grad1 .* grad1))
        mhat1 = m1/(1- (beta1^index))
        vhat1 = v1 / (1 - (beta2^index))
        weight1 = weight1 - ((alpha .* mhat1) ./ (sqrt.(vhat1) .+ E))

        
        # update layer 2 weights
        m2 = (beta1 * m2) + ((1-beta1) * grad2)
        v2 = (beta2 * v2) + ((1-beta2) * (grad2 .* grad2))
        mhat2 = m2/(1- (beta1^index))
        vhat2 = v2 / (1 - (beta2^index))
        old_weight2 = weight2
        weight2 = weight2 - ((alpha .* mhat2) ./ (sqrt.(vhat2) .+ E))

        stop_condition = abs(norm(old_weight2) - norm(weight2))

        println("Difference between old and new weight vectors ",stop_condition)
        if (stop_condition < 10^-5 && (index > max_iteration))
            stop = true
        end
        temp = test(weight1, weight2, input, target, testidx)
        push!(error, temp)

        println("Batch Training Error = $(test(weight1, weight2, input, target, trainidx))")
        
       index += 1 
    end

    error = test(weight1, weight2, input, target, trainidx)
    println("Final Training Error = $(error)")
    
    error = test(weight1, weight2, input, target, testidx)
    println("Final Validation Error = $(error)")
    
    return weight1, weight2, test(weight1, weight2, input, target, testidx)
end



function demo()
    l = Float64[]
    i = []
    h5open("mnist.h5", "r") do file
        labels = read(file, "train/labels")
        images = read(file, "train/images")
        len = size(labels)[1]
        for x = 1:len

            push!(l,labels[x])
            img = images[:,:,x]
            new_img = reshape(img, 784)
            
            push!(i,new_img)
        end
        
      
        w1, w2, err = train(i, l)
    end
    
end

# Getting the trained first and second weight vectors to store them in the weights h5 file
# to use them in the error rate function ERF

w1, w2, e = @time demo()
h5open("weights.h5", "w") do file
    write(file, "weight1", w1)
    write(file, "weight2", w2)
end
